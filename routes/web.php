<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('inventario','InventarioController@index')->name('inventario.index');
Route::get('inventario/create','InventarioController@create')->name('inventario.create');
Route::post('inventario/create','InventarioController@store')->name('inventario.store');
Route::get('inventario/edit/{id}','InventarioController@edit')->name('inventario.edit');
Route::put('inventario/update/{id}','InventarioController@update')->name('inventario.update');
Route::delete('inventario/destroy/{id}','InventarioController@destroy')->name('inventario.destroy');
Route::get('inventario/show/{id}','InventarioController@show')->name('inventario.show');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');