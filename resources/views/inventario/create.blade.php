@extends('layout.layout')
@section('content')
	<div class="container">
		 @if ($errors->any())
	      <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	      </div><br />
	    @endif
		<form method="post" action="{{ route('inventario.store') }}" enctype="multipart/form-data">
			 @csrf
		  <div class="form-group">
		    <label for="formGroupExampleInput">Código</label>
		    <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Introduce el código aquí">
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Nombre</label>
		    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Introduce el nombre aquí">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlTextarea1">Descripción</label>
		    <textarea class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Cantidad</label>
		    <input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Introduce la cantidad aquí">
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Precio</label>
		    <input type="text" class="form-control" id="precio" name="precio" placeholder="Introduce el precio aquí">
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Proveedor</label>
		    <input type="text" class="form-control" id="proveedor" name="proveedor" placeholder="Introduce el proveedor aquí">
		  </div>
		  <div class="form-group">
		  	<input type="file" name="fichero" id="fichero">
		  </div>
		  <button type="submit" class="btn btn-primary offset-5 mb-2">Guardar</button>
		</form>
	</div>
</body>
</html>
@endsection